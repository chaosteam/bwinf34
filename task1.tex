\section*{Aufgabe 1}
\subsection*{Lösungsidee}
In der ersten Teilaufgabe soll festgestellt werden, ob Kassiopeia ausgehend von der Startposition überhaupt alle weißen Felder erreichen kann.
Dabei spielt es zunächst noch keine Rolle, ob es für Kassiopeia auch einen Weg über alle Felder gibt, bei dem sie jedes Feld nur einmal besucht.
Im Folgenden werden die weißen, an ein anderes Feld angrenzenden und damit von Kassiopeia in einem Schritt erreichbaren Felder als Nachbarn dieses Felds bezeichnet.

Um diese Aufgabe zu lösen werden zuerst alle Felder markiert, die durch Kassiopeia erreichbar sind.
Am Ende kann dann geprüft werden, ob alle weißen Felder markiert wurden.
Zum Markieren aller erreichbarer Felder wird bei dem Feld, auf welchem Kassiopeia startet, begonnen.
Als Nächstes werden alle nicht-markierten Nachbarn dieses Feldes markiert, denn auch diese kann Kassiopeia natürlich erreichen.
Auch die Nachbarn aller dieser Felder werden danach markiert, usw.\ bis am Ende keine nicht-markierten Nachbarn mehr existieren.
Wenn nun alle weißen Felder markiert sind, dann kann Kassiopeia alle weißen Felder erreichen, sonst nicht.
Dieser Algorithmus terminiert, denn in jedem Schritt werden Felder markiert, welche danach nicht mehr bearbeitet werden.

Die Erreichbarkeit aller Felder ist zwar eine Voraussetzung dafür, dass der in der zweiten Teilaufgabe gesuchte Weg über alle Felder ohne doppeltes Betreten eines Feldes existiert, reicht aber noch nicht aus.
Nachdem die Erreichbarkeit geprüft wurde, muss deshalb noch nach so einem Weg gesucht werden.
Das lässt sich durch einen rekursiven Algorithmus realisieren.
Dazu wird mit dem Startfeld begonnen. 
Es muss auf jedem Fall zum gesuchten Weg gehören.
Danach gibt es mehrere Möglichkeiten für Kassiopeia, ihren Weg fortzusetzen: Sie kann jedes Nachbarfeld als Nächstes betreten.
Deswegen wird für jede dieser Möglichkeiten durch Rekursion überprüft, ob es denn für diesen Weganfang eine mögliche Fortsetzung gibt, welche alle weißen Felder besucht.
Es wird nie ein Feld erneut betreten, welches sich bereits im aktuellen Weganfang befindet.
Somit wird die Terminierung gesichert, denn der Weganfang wächst mit jedem Rekursionsschritt und kann nie mehr als alle Felder enthalten.
Sobald eine Fortsetzung gefunden wurde, welche alle verbleibenden Felder besucht, wird diese zurückgegeben.
Falls keine unbesuchten Nachbarn mehr existieren, wird entweder ein leerer Weg (wenn keine noch zu besuchenden weißen Felder mehr existieren) oder ein Fehler zurückgegeben, welcher der vorherigen Stufe signalisiert, dass dieser Weganfang nicht zu einer Lösung führt.
Da so alle möglichen Wege durchsucht werden, findet der Algorithmus immer eine Lösung, falls eine existiert. 

\subsection*{Implementierung}
Die Lösung ist in \verb|Haskell| implementiert und besteht aus zwei Dateien: \verb|Kassiopeia.hs| und \verb|task1.hs|.
\verb|Kassiopeia.hs| enthält dabei den Hauptteil des Algorithmus.
Zunächst wird dafür ein Datentyp definiert, welcher die bestimmenden Daten eines Kassiopeia-Problems speichert:
\begin{haskellcode}
-- | A position on the board. (0,0) is the north-west corner.
type Position = (Int, Int)

-- | Parameters of a single Kassiopeia problem.
data Problem = Problem
  { -- | True for each field Kassiopeia needs to visit
    board :: UArray Position Bool 
    -- | Positions where Kassiopeia's path may start
  , start :: [Position]           
  } deriving (Eq, Show)
\end{haskellcode}
Dieser Datentyp erlaubt mehrere Startpositionen, was später die Implementierung erleichtert.
Danach werden Funktionen zum Einlesen der Problemdaten aus dem in der Aufgabenstellung spezifizierten Format definiert. 
Die Hauptfunktion ist hier \verb|parseProblem|, welche einen \verb|String| mit den Daten des Problems erhält und daraus ein \verb|Problem| erzeugt.

Der eigentliche Algorithmus in mit den Funktionen \verb|solvable| und \verb|paths| implementiert.
Beide Algorithmen funktionieren nach dem Prinzip, dass das Problem in Teilprobleme aufgespalten wird, welche rekursiv gelöst werden.
Dies geschieht solange, bis ein trivial lösbares Problem erreicht wird.
Die folgenden zwei Funktionen testen auf solche trivialen Probleme: entweder es wurden alle weißen Felder besucht (\verb|success| ist \verb|True|, die Lösung ist in diesem Fall der leere Pfad) oder es gibt keine Felder mehr, die wir besuchen könnten (\verb|possiblySolvable| ist \verb|False|, das Problem ist unlösbar).
Dabei ist zu beachten, dass \verb|success| immer vor \verb|possiblySolvable| überprüft wird, denn \verb|possiblySolvable| kann auch \verb|False| sein, wenn gar keine Felder mehr zu besuchen sind.
\begin{haskellcode}
-- | Return True if a problem is solved successfully.
success :: Problem -> Bool
success = all not . elems . board

-- | Return True only if progress can be made solving the problem. 
possiblySolvable :: Problem -> Bool
possiblySolvable = not . null . start
\end{haskellcode}

Mithilfe dieser Funktionen lässt sich nun \verb|solvable| definieren.
\verb|solvable| gibt genau dann \verb|True| zurück, wenn Kassiopeia alle Felder erreichen kann (Teil 1 der Aufgabe).
\begin{haskellcode}
-- | Return True only if Kassiopeia can visit all required fields.
--
-- Note: This does not mean that there is a path that visits each field only once.
solvable :: Problem -> Bool
solvable problem = success problem || possiblySolvable pruned && solvable (visitAll pruned)
 where
  pruned = pruneStart problem
\end{haskellcode}

Die Funktion überprüft zunächst die beiden Abbruchbedingungen und gibt in diesem Fall direkt entweder \verb|True| oder \verb|False| zurück, wenn wir sicher wissen, dass das Problem lösbar bzw.\ unlösbar ist.
Sonst werden zunächst überflüssige Startpositionen, welche bereits nicht mehr besucht werden müssen, entfernt.
Das wird von der \verb|pruneStart| Funktion erledigt.
Diese ist wie folgt definiert:
\begin{haskellcode}
-- | Return True if the given position is in the bounds of the board.
inBoard :: Problem -> Position -> Bool
inBoard = inRange . bounds . board

-- | Return True if the position still needs to be visited. Returns False for positions
-- outside of the bounds of the board.
needVisit :: Problem -> Position -> Bool
needVisit problem position = inBoard problem position && board problem ! position 

-- | Filter start positions which need not be visited anymore.
pruneStart :: Problem -> Problem
pruneStart p = p { start = filter (needVisit p) (start p) }
\end{haskellcode}

Dann wird das neue Teilproblem generiert und rekursiv gelöst. 
Dazu werden die aktuellen Startpositionen als besucht markiert und alle Nachbarfelder der aktuellen Startpositionen als neue Startpositionen festgelegt.
Dieser Prozess ist in der \verb|visitAll| Funktion implementiert:
\begin{haskellcode}
-- | Mark all current starting positions as visited.
visitStart :: Problem -> Problem
visitStart p = p { board = board p // [(pos, False) | pos <- start p] }

-- | Replace each starting position with the neighbours of that position.
startNeighbours :: Problem -> Problem
startNeighbours p = p { start = concatMap neighbours (start p) }

-- | Return all neighbours of the given position.
neighbours :: Position -> [Position]
neighbours (x,y) = [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]

-- | Visit all the current start positions, and add the neighbours as new start positions.
visitAll :: Problem -> Problem
visitAll = startNeighbours . visitStart 
\end{haskellcode}

Nun fehlt nur noch die Funktion zum Finden eines Pfads, der jedes Feld genau einmal besucht.
In der Implementierung trägt diese den Namen \verb|paths|, und ist wie folgt implementiert:
\begin{haskellcode}
-- | Find all paths which visit every field only once and start at any of the starting
-- positions.
paths :: Problem -> [[Position]]
paths problem 
  | success problem = pure []
  | otherwise = do
    pos <- start $ pruneStart problem
    rest <- paths (visitAll (problem { start = [pos] }))
    pure $ pos : rest 
\end{haskellcode}
Diese Funktion findet rekursiv alle Wege für Kassiopeia, bei denen jedes weiße Feld genau einmal besucht wird.
Da \verb|Haskell| lazy ist, kann diese Funktion auch effizient zum Finden eines einzigen Pfads verwendet werden.
In diesem Fall wird Haskell nur so viel berechnen, wie für die Bestimmung des ersten Pfads benötigt wird.

Zuerst wird überprüft, ob Kassiopeia bereits alle weißen Felder besucht hat.
Sollte dies der Fall sein, kann einfach ein leerer Pfad zurückgegeben werden, denn es müssen ja keine weiteren Felder besucht werden.
Sonst wird für jede mögliche Startposition überprüft, ob ein Pfad mit dieser Position am Anfang existiert.
Dazu wird die Position als besucht markiert und die Nachbarn werden als neue mögliche Startpositionen festgelegt.
Dann ruft sich die Funktion rekursiv selbst auf.

\verb|task1.hs| kümmert sich um das Einlesen der Eingabedaten und die Ausgabe der Lösung.
Dazu definiert es die \verb|main| Funktion und eine Hilfsfunktion zur Konvertierung der Ausgabe in das gewünschte Format:
\begin{haskellcode}
import Kassiopeia

-- | Formats a path of entries given in the form `(fromPosition, toPosition)` to match
-- the expected output of the BwInf task.
formatPath :: [(Position, Position)] -> [Char]
formatPath = map (formatTransition . difference)
 where
  difference ((fx, fy), (tx, ty)) = (tx - fx, ty - fy)
  formatTransition (-1,  0) = 'N'
  formatTransition (1 ,  0) = 'S'
  formatTransition ( 0, -1) = 'W'
  formatTransition ( 0,  1) = 'O'
  formatTransition _        = error "internal error: program produced invalid path"

main :: IO ()
main = do
  problem <- fmap (parseProblem . lines) getContents
  if solvable problem
    then case paths problem of
      []  -> putStrLn "alle Felder erreichbar, aber kein Weg möglich"
      x:_ -> putStrLn (formatPath (zip x (drop 1 x)))
    else putStrLn "kann nicht alle Felder erreichen"
\end{haskellcode}

\subsection*{Beispiele}
Im Folgenden sind die Ergebnisse für die auf der \verb|BwInf|-Homepage abrufbaren Beispiele aufgeführt.
Das Programm erwartet die Eingabe über Standardeingabe entsprechend dem Dateiformat der \verb|BwInf|-Beispiele.
Wenn ein Weg möglich ist, wird dieser wie gefordert als Abfolge von W, N, O oder S für Westen, Norden, Osten bzw.\ Süden ausgegeben.
Ansonsten wird entweder ``alle Felder erreichbar, aber kein Weg möglich'' oder ``kann nicht alle Felder erreichen'' ausgegeben, je nach dem, ob Kassiopeia wenigstens alle weißen Felder erreichen kann oder nicht.

\begin{minted}{bash}
$> ./task1 < kassiopeia0.txt
WNNWSSSOOONNNOOSSSONNN
$> ./task1 < kassiopeia1.txt
kann nicht alle Felder erreichen
$> ./task1 < kassiopeia2.txt
alle Felder erreichbar, aber kein Weg möglich
$> ./task1 < kassiopeia3.txt
WWSSOOOONOSONNWWW
$> ./task1 < kassiopeia4.txt
alle Felder erreichbar, aber kein Weg möglich
$> ./task1 < kassiopeia5.txt
WWWWWWWWWWWW
$> ./task1 < kassiopeia6.txt
alle Felder erreichbar, aber kein Weg möglich
$> ./task1 < kassiopeia7.txt
alle Felder erreichbar, aber kein Weg möglich
\end{minted}
