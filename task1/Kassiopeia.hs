module Kassiopeia where

import Data.Array.Unboxed
import Data.Bifunctor (second)
import Data.List (find)

-- | A position on the board. (0,0) is the north-west corner.
type Position = (Int, Int)

-- | Parameters of a single Kassiopeia problem.
data Problem = Problem
  { board :: UArray Position Bool -- ^ True for each field Kassiopeia needs to visit
  , start :: [Position]           -- ^ Positions where Kassiopeia's path may start
  } deriving (Eq, Show)

-- | Parse a single field of Quadratien.
parseField :: Char -> Bool
parseField '#' = False
parseField ' ' = True
parseField 'K' = True
parseField x   = error $ "invalid leaf: " ++ [x]

-- | Parse a whole board, by parsing each field with 'parseField'.
parseBoard :: [(Position, Char)] -> [(Position, Bool)]
parseBoard = map (second parseField) 

-- | Parse the bounds of the Quadration board from the header.
-- Returns 0-indexed bounds.
parseBounds :: [String] -> (Position, Position)
parseBounds [height, width] = ((0, 0), (read height - 1, read width - 1))
parseBounds ws = error $ "invalid header: expected 2 words but got: " ++ show (length ws)

-- | Parse a problem input from a list of input lines. The first line should be the header,
-- the following ones should be board data.
parseProblem :: [String] -> Problem
parseProblem [] = error "invalid input: missing header"
parseProblem (header:boardData) = Problem (array bs (parseBoard indexed)) [startPos]
 where
  indexed = concat $ zipWith (\i -> zip [(i,j) | j <- [0..]]) [0..] boardData
  bs = parseBounds (words header)
  startPos = maybe (error "No starting position") fst maybeStartField
  maybeStartField = find (('K' ==) . snd) indexed

-- | Return True if a problem is solved successfully.
success :: Problem -> Bool
success = all not . elems . board

-- | Return True only if progress can be made solving the problem. 
possiblySolvable :: Problem -> Bool
possiblySolvable = not . null . start

-- | Return True if the given position is in the bounds of the board.
inBoard :: Problem -> Position -> Bool
inBoard = inRange . bounds . board

-- | Return True if the position still needs to be visited. Returns False for positions
-- outside of the bounds of the board.
needVisit :: Problem -> Position -> Bool
needVisit problem position = inBoard problem position && board problem ! position 

-- | Filter start positions which need not be visited anymore.
pruneStart :: Problem -> Problem
pruneStart p = p { start = filter (needVisit p) (start p) }

-- | Return all neighbours of the given position.
neighbours :: Position -> [Position]
neighbours (x,y) = [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]

-- | Mark all current starting positions as visited.
visitStart :: Problem -> Problem
visitStart p = p { board = board p // [(pos, False) | pos <- start p] }

-- | Replace each starting position with the neighbours of that position.
startNeighbours :: Problem -> Problem
startNeighbours p = p { start = concatMap neighbours (start p) }

-- | Visit all the current start positions, and add the neighbours as new start positions.
visitAll :: Problem -> Problem
visitAll = startNeighbours . visitStart 

-- | Return True only if Kassiopeia can visit all required fields.
--
-- Note: This does not mean that there is a path that visits each field only once.
solvable :: Problem -> Bool
solvable problem = success problem || possiblySolvable pruned && solvable (visitAll pruned)
 where
  pruned = pruneStart problem

-- | Find all paths which visit every field only once and start at any of the starting
-- positions.
paths :: Problem -> [[Position]]
paths problem 
  | success problem = pure []
  | otherwise = do
    pos <- start $ pruneStart problem
    rest <- paths (visitAll (problem { start = [pos] }))
    pure $ pos : rest 
