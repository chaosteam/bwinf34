# Testfälle für das Kassiopeia-Programm

Die Testfälle sind in 3 Klassen aufgeteilt:

1. unsolvable: Für diese Konfiguration sind nicht alle Felder erreichbar, demanch existiert hier natürlich auch kein Weg für Kassiopeia
2. reachable: Hier sind zwar alle Felder erreichbar, aber es existiert kein Weg für Kassiopeia
3. solveable: Für diese Testfälle schließlich sind sowohl alle Felder erreichbar, und es existiert ein Weg für Kassiopeia
