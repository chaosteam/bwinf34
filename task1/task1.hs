module Main where

import Kassiopeia

-- | Formats a path of entries given in the form `(fromPosition, toPosition)` to match
-- the expected output of the BwInf task.
formatPath :: [(Position, Position)] -> [Char]
formatPath = map (formatTransition . difference)
 where
  difference ((fx, fy), (tx, ty)) = (tx - fx, ty - fy)
  formatTransition (-1,  0) = 'N'
  formatTransition (1 ,  0) = 'S'
  formatTransition ( 0, -1) = 'W'
  formatTransition ( 0,  1) = 'O'
  formatTransition _        = error "internal error: program produced invalid path"

main :: IO ()
main = do
  problem <- fmap (parseProblem . lines) getContents
  if solvable problem
    then case paths problem of
      []  -> putStrLn "alle Felder erreichbar, aber kein Weg möglich"
      x:_ -> putStrLn $ formatPath (zip x (drop 1 x))
    else putStrLn "kann nicht alle Felder erreichen"
