{-# LANGUAGE RecordWildCards #-}
module Main where

import Kassiopeia
import Control.Monad (forM_, filterM)
import Data.List (sort, isSuffixOf, group)
import System.Directory (getDirectoryContents, doesFileExist)
import System.FilePath
import Test.Tasty
import Test.Tasty.HUnit

tests :: Inputs -> TestTree
tests Inputs{..} = testGroup "Kassiopeia"
  [ testGroup "unsolvable" (map (test False False) unsolvableInputs)
  , testGroup "reachable" (map (test True False) reachableInputs)
  , testGroup "solvable" (map (test True True) solvableInputs)
  ]

test :: Bool -> Bool -> FilePath -> TestTree
test isSolvable hasPaths file = testCase file $ do
  input <- lines <$> readFile file
  let problem = parseProblem input
      ps = paths problem
  assertEqual "solvable" isSolvable (solvable problem)
  assertEqual "paths not null" hasPaths (not (null ps))
  forM_ (take 1 ps) $ \p -> do
    assertBool "path contains no duplicates" $ noDuplicates p
    assertBool "path covers every field" $ covers problem p
    assertBool "path entries are neighbours" $ all isNeighbour (zip p (drop 1 p))

noDuplicates :: (Ord a, Eq a) => [a] -> Bool
noDuplicates = all (null . drop 1) . group . sort

covers :: Problem -> [Position] -> Bool
covers problem path = success (visitStart (problem { start = path }))

isNeighbour :: (Position, Position) -> Bool
isNeighbour ((x1, y1), (x2, y2)) = abs (x2 - x1) + abs (y2 - y1) == 1

data Inputs = Inputs
  { solvableInputs :: [FilePath]
  , reachableInputs :: [FilePath]
  , unsolvableInputs :: [FilePath] 
  }

getClassInputs :: String -> IO [String]
getClassInputs c = do
  let prefix = "data" </> c
  entries <- map (prefix </>) <$> getDirectoryContents prefix
  filterM doesFileExist (filter (".txt" `isSuffixOf`) entries)

main :: IO ()
main = do
  solvableInputs <- getClassInputs "solvable"
  reachableInputs <- getClassInputs "reachable"
  unsolvableInputs <- getClassInputs "unsolvable"
  defaultMain $ tests Inputs{..}
