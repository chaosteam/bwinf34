package ameisenfutter;

import ameisenfutter.simulation.Coordinate;
import ameisenfutter.simulation.Simulation;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author jan
 */
public final class Ameisenfutter extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private int updateDuration = 100;
    private Simulation simulation;
    
    @Override
    public void start(Stage primaryStage) {
        simulation = Simulation.createSimulation(100, 6, new Coordinate(250, 250), 250, new Coordinate(500, 500));
        primaryStage.setTitle("Ameisenfutter");
        
        Group root = new Group();
        root.setOnKeyTyped(eh -> {
            if(eh.getCharacter().equals("s")){
                createSettingsDialog();
            }
        });
        Canvas canvas = new Canvas(500, 500);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        canvas.widthProperty().bind(root.getScene().widthProperty());
        canvas.heightProperty().bind(root.getScene().heightProperty());
        Platform.runLater(() -> {root.requestFocus();});
        
        AnimationTimer timer = new AnimationTimer() {
            private long lastUpdate = System.nanoTime();
            @Override
            public void handle(long now) {
                while(now - lastUpdate >= updateDuration*1000000){ // every 1ms a step
                    lastUpdate += updateDuration*1000000;
                    simulation.update();
                    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                    drawState(gc, simulation);
                }
            }
        };
        timer.start();
    }
    
    private void drawState(GraphicsContext gc, Simulation s) {
        
        double scaleX = gc.getCanvas().getWidth()/s.getEnvironment().getWideX();
        double scaleY = gc.getCanvas().getHeight()/s.getEnvironment().getWideY();
        gc.fillText("Press s for Settings", 10d, gc.getCanvas().getHeight()-10d);
        
        gc.setFill(Color.GREEN);
        gc.fillRect(scaleX*s.getEnvironment().getNest().getX(), scaleY*s.getEnvironment().getNest().getY(), scaleX, scaleY);
        
        gc.setFill(Color.RED);
        s.getEnvironment().getFoodSources().forEach((c,p) -> {
            if(p > 0){
                gc.fillRect(scaleX*c.getX(), scaleY*c.getY(), scaleX, scaleY);
            }
        });
        gc.setFill(Color.CYAN);
        s.getEnvironment().getMarkedFields().forEach((c,p) -> {
            gc.fillRect(scaleX*c.getX(), scaleY*c.getY(), scaleX, scaleY);
        });
        gc.setFill(Color.BLACK);
        s.getAnts().stream().forEach((a) -> {
            gc.fillRect(scaleX*a.getPosition().getX(), scaleY*a.getPosition().getY(), scaleX, scaleY);
        });
    }
    
    private void createSettingsDialog(){
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("SettingsDialog.fxml"));
        GridPane grid = null;
        try {
            grid = (GridPane)loader.load();
        } catch (IOException ex) {
            Logger.getLogger(Ameisenfutter.class.getName()).log(Level.SEVERE, "Can't load SettingsDialog!", ex);
            return;
        }
        Dialog<Map<String, Object>> dialog = new Dialog<>();
        dialog.setTitle("Einstellungen");
        dialog.setHeaderText("");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.APPLY, ButtonType.CANCEL);
        grid.getChildren().filtered(n -> n instanceof TextField).forEach(n -> {
            TextField f = (TextField) n;
            switch(f.getId()){
                case "updateDuration" : f.setText(""+updateDuration); break;
                case "evapDuration" : f.setText(""+simulation.getEvapDuration()); break;
                case "foodCount" : f.setText(""+simulation.getEnvironment().getFoodSources().keySet().size()); break;
                case "antCount" : f.setText(""+simulation.getAnts().size()); break;
                case "fieldSize" : f.setText("["+simulation.getEnvironment().getWideX()+";"+simulation.getEnvironment().getWideY()+"]"); break;
                case "nestPos" : f.setText(simulation.getEnvironment().getNest().toString());
            }
        });
        dialog.getDialogPane().setContent(grid);
        dialog.setResultConverter(b -> {
            if(b == ButtonType.APPLY){
                try{
                    HashMap<String, Object> result = new HashMap<>();
                    ((GridPane)dialog.getDialogPane().getContent()).getChildren().filtered(n -> n instanceof TextField).forEach(n -> {
                        TextField f = (TextField) n;
                        switch(f.getId()){
                            case "updateDuration" :
                            case "evapDuration" :
                            case "foodCount" :
                            case "antCount" : result.put(f.getId(), Integer.valueOf(f.getText())); break;
                            case "fieldSize" :
                            case "nestPos" : 
                                String[] coords = f.getText().replaceAll("[^0-9;]+", "").split(";"); 
                                result.put(f.getId(), new Coordinate(Integer.parseInt(coords[0]), Integer.parseInt(coords[1])));
                        }
                    });
                    return result;
                }catch (Exception ex){
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Wrong Input Error");
                    alert.setHeaderText("Failed to parse input!");
                    alert.setContentText(ex.getMessage());
                    alert.showAndWait();
                }
            }
            return null;
        });
        Optional<Map<String, Object>> result = dialog.showAndWait();
        if(result.isPresent()){
            Map<String, Object> settings = result.get();
            updateDuration = (int) settings.get("updateDuration");
            simulation = Simulation.createSimulation(
                    (int) settings.get("antCount"),
                    (int) settings.get("foodCount"), 
                    (Coordinate)settings.get("nestPos"),
                    (int) settings.get("evapDuration"), 
                    (Coordinate)settings.get("fieldSize"));
        }
    }
}
