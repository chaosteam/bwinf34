package ameisenfutter.simulation;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jan
 */
public final class Ant {
    
    private final Environment environment;
    private final Coordinate startField;
    
    private Coordinate position;
    private Coordinate lastField;
    private Coordinate lastFoodSource;
    
    private boolean seeking = true;
    
    
    private Ant(){
        environment = null;
        startField = null;
    }
    
    private Ant(Environment environment){
        this.environment = environment;
        Coordinate nest = environment.getNest();
        this.lastField = nest;
        this.startField = nest;
        this.position = nest;
    }
    
    public static Ant createAnt(Environment environment){
        return new Ant(environment);
    }
    
    public Coordinate getPosition(){
        return position;
    }
    
    void move(){
        if(seeking){
            //add all fields around the ant
            List<Coordinate> coords = new LinkedList<>(Arrays.asList(new Coordinate[]{
                new Coordinate(position.getX()-1, position.getY()),
                new Coordinate(position.getX()+1, position.getY()),
                new Coordinate(position.getX(), position.getY()-1),
                new Coordinate(position.getX(), position.getY()+1)
                }));
            //remove field which the ant comes from
            coords.remove(lastField);
            //remove fields which are out of the bounds of the environment
            coords.removeIf(p -> p.getX() <= 0 || p.getY() <= 0 || p.getX() >= environment.getWideX() || p.getY() >= environment.getWideY() );
            //order the fields in a random way
            Collections.shuffle(coords);
            int lastPheromonIntensity = -1;
            lastField = position;
            for(Coordinate c : coords){
                //if there is a food source go to that field, take a portion and
                //do not check the other fields. Change the mode to not seeking
                //because the ant should return to its nest
                if(!environment.isEmty(c)){
                    position = c;
                    environment.take(c);
                    lastFoodSource = position;
                    seeking = false;   
                    position = c;
                    return;
                }
                //check if this field contains the highest pheromonintenisity around
                if(environment.getPheromoneIntensity(c) > lastPheromonIntensity){
                    lastPheromonIntensity = environment.getPheromoneIntensity(c);
                    position = c;
                }
            }
        }else{
            lastField = position;
            float dx = lastFoodSource.getX() - startField.getX();
            float dy = lastFoodSource.getY() - startField.getY();
            float nowDy = position.getY() - startField.getY();
            float nowDx = position.getX() - startField.getX();
            if(dy == 0f){
                position = new Coordinate(position.getX()-((int)Math.signum(dx)), position.getY());
            }else if(dx == 0f){
                position = new Coordinate(position.getX(), position.getY()-((int)Math.signum(dy)));
            }else{
                //if the ant is above the direct line and the nest it goes down
                //if the ant is under the direct line and the nest it goes up
                if(Math.abs(nowDx)*(Math.abs(dy)/Math.abs(dx)) < Math.abs(nowDy)){
                    position = new Coordinate(position.getX(), position.getY()-((int)(Math.signum(dy))));
                }else{
                    //if the ant is left of the nest it goes right otherwise it goes left
                    position = new Coordinate(position.getX()-((int)(Math.signum(dx))), position.getY());
                }
            }
            //set mode to seeking if it has reached the nest
            if(position.equals(startField)){
                seeking = true;
                return;
            }
            //mark the field because it is on the way to its nest
            environment.mark(position);
        }
    }
    

}
