package ameisenfutter.simulation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jan
 */
public final class Environment {
    
    private final int wideX;
    private final int wideY;
    private final Coordinate nest;
    private final Map<Coordinate, Integer> foodsources;
    private final Map<Coordinate, Integer> markedFields;
    
    private Environment(){ 
        this.wideX = 0;
        this.wideY = 0;
        this.nest = null;
        this.foodsources = Collections.EMPTY_MAP;
        this.markedFields = Collections.EMPTY_MAP;
    }
    
    private Environment(Coordinate nest, Map<Coordinate, Integer> foodsources, 
            int wideX, int wideY){
        this.nest = nest;
        this.foodsources = foodsources;
        this.wideX = wideX;
        this.wideY = wideY;
        this.markedFields = new HashMap<>();
    }
    
    public static Environment createEnvironment(
            Coordinate nest, Map<Coordinate, Integer> foodsources, 
            int sizeX, int sizeY){
        return new Environment(nest, foodsources, sizeX, sizeY);
    }
    
    public Coordinate getNest(){
        return this.nest;
    }
    
    public int getPheromoneIntensity(Coordinate c){
        Integer result = markedFields.get(c);
        return result == null ? 0 : result;
    }
    
    
    void mark(Coordinate coord){
        markedFields.compute(coord, (c, i) -> i == null ? 1 : i + 1);
    }
    
    public boolean isEmty(Coordinate c){
        Integer size = foodsources.get(c);
        return size == null ? true : size == 0;
    }
    
    public int getWideX(){
        return wideX;
    }
    
    public int getWideY(){
        return wideY;
    }
    
    void take(Coordinate coord){
        if(isEmty(coord))
            throw new IllegalArgumentException("Coordinate "+coord+" is emty!");
        foodsources.computeIfPresent(coord, (c, i) -> --i);
    }
    
    void evaporate(){
        markedFields.replaceAll((c, i) -> --i);
        markedFields.values().removeIf(i -> i <= 0);
    }
    
    //needed for straightforward implementation of visualization
    
    public Map<Coordinate, Integer> getMarkedFields(){
        return Collections.unmodifiableMap(markedFields);
    }
    
    public Map<Coordinate, Integer> getFoodSources(){
        return Collections.unmodifiableMap(foodsources);
    }
    
}
