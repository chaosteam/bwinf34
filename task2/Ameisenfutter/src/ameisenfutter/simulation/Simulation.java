package ameisenfutter.simulation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jan
 */
public final class Simulation {
    
    
    public static Simulation createSimulation(int antCount, int foodCount, Coordinate nestPos, int evapRate, Coordinate fieldSize){
        //add foodsources at random locations
        Map<Coordinate, Integer> foodsources = new HashMap<>(foodCount);
        for(int i = 0; i < foodCount; ++i){
            int x = (int) (Math.random() * (fieldSize.getX()-1))+1;
            int y = (int) (Math.random() * (fieldSize.getY()-1))+1;
            Coordinate c = new Coordinate(x, y);
            //don't place foodsource at nest positon
            if(nestPos.equals(c)){
                --i;
                continue;
            }
            foodsources.put(c, /*SIZE_FOODSOURCE*/ 50);
        }
        
        Environment environment = Environment.createEnvironment(nestPos, foodsources, fieldSize.getX(), fieldSize.getY());
        
        List<Ant> ants = new ArrayList<>(antCount);
        for(int i = 0; i < antCount; ++i){
            ants.add(Ant.createAnt(environment));
        }
        
        return new Simulation(environment, evapRate, ants);
    }
    
    private final int evapDuration;
    private final List<Ant> ants;
    private final Environment environment;
    
    private Simulation(){
        this(null, 0, new ArrayList<>(0));
    }
    
    private Simulation(Environment env, int evapDuration, List<Ant> ants ){
        this.environment = env;
        this.ants = ants;
        this.evapDuration = evapDuration;
    }
    
    private int nextEvap = 0;
    
    public void update(){
        --nextEvap;
        if(nextEvap <= 0){
            environment.evaporate();
            nextEvap = evapDuration;
        }
        ants.stream().forEach((a) -> a.move() );
    }
    
    public Environment getEnvironment(){
        return environment;
    }
    
    public List<Ant> getAnts(){
        return Collections.unmodifiableList(ants);
    }
    
    public int getEvapDuration(){
        return evapDuration;
    }
}
