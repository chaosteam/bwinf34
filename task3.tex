\section*{Aufgabe 3}
\subsection*{Lösungsidee}
Im Folgenden handelt es sich bei Verteilungen stets um unterscheidbare Verteilungen.
Es sei \(z\) die An\textbf{z}ahl der Flaschen.
Die Behälter seinen mit natürlichen Zahlen beginnend mit \(0\) durchnummeriert und \(g_k\) sei die \textbf{G}röße des Behälters \(k\), welche die maximale Anzahl an Flaschen angibt, die in den Behälter \(k\) passen.
Die Sequenz \(G(n+1) = \langle g_0, g_1, \ldots, g_{n-1}, g_n \rangle\) enthalte dann die Größen der Behälter \(0\) bis \(n\).
Somit ist \(G(0) = \langle \rangle\) die leere Sequenz.
Gesucht ist nun eine Funktion \(v(z, G(n))\), welche die Anzahl aller \textbf{V}erteilungen von \(z\) Flaschen auf \(n\) Behälter mit den angegebenen Größen berechnet.

\subsubsection*{Der naive Algorithmus}
Laut Aufgabenstellung sind die Behälter voneinander unterscheidbar während die Flaschen nicht unterscheidbar sind.
Daraus folgt das zwei Verteilungen genau dann unterschiedlich sind, wenn die Anzahl der Flaschen in mindestens einem Behälter bei den Verteilungen unterschiedlich sind.
%Die Anzahl aller Verteilungen ist demnach gleich der Summe der Anzahlen von Verteilungen, bei denen die Anzahl der Flaschen in einem Behälter zwischen allen möglichen Werten variiert.
Somit muss folgende Gleichung gelten:
\begin{equation}
  \label{equation:recursion}
  v(z, G(n+1)) = \sum\limits_{i = 0}^{g_n} v(z - i, G(n))
\end{equation}
Jeder Term der Summe steht für die Anzahl der Verteilungen bei denen im Behälter \(n\) genau \(i\) Flaschen sind.
Diese Anzahl ist gleich der Anzahl von Verteilungen von \(z - i\) Flaschen auf die restlichen \(n\) Behälter (\(0\) bis \(n - 1\)), da es nur eine Möglichkeit gibt, \(i\) Flaschen in den Behälter \(n\) zu packen.
Die Anzahl der Flaschen im Behälter \(n\) muss laut Aufgabenstellung zwischen \(0\) und \(g_n\) Flaschen liegen, wodurch sich die Grenzen für \(i\) ergeben.

Die Gleichung lässt sich auch als rekursive Berechnungsvorschrift für \(v\) lesen.
Dabei verringert sich die Anzahl der Behälter in jedem Rekursionsschritt um eins.
Somit wird schließlich die Anzahl von Verteilungen auf null Behälter benötigt.
Dafür gilt die folgende Gleichung:
\begin{equation}
  \label{equation:base}
  v(z, G(0)) = \begin{cases}
    1 & \text{falls } z = 0 \\
    0 & \text{falls } z \neq 0
  \end{cases}
\end{equation}
Es gibt genau eine (triviale) Verteilung von null Flaschen in null Behälter.
Mehrere Flaschen lassen sich dagegen überhaupt nicht in null Behälter verteilen.

Zusammen definieren Gleichung (\ref{equation:recursion}) und (\ref{equation:base}) einen naiven Algorithmus zum Berechnen der Anzahl aller Verteilungen.
Dazu werden jeweils rekursiv alle Zwischenwerte berechnet und anschließend addiert.
Allerdings werden dabei viele Werte mehrfach berechnet, wie ein Beispiel mit 2 Flaschen und 2 Behältern mit den Größen 1 und 3 zeigt:
\begin{align*}
  v(2, (1,3)) &= v(2, \langle 1 \rangle) + v(1, \langle 1 \rangle) + v(0, \langle 1 \rangle) \\
              &= [ v(2, \langle \rangle) + v(1, \langle \rangle) ] + [ v(1, \langle \rangle) + v(0, \langle \rangle) ] + [ v(0, \langle \rangle) + v(-1, \langle \rangle) ] \\
              &= [0 + 0] + [0 + 1] + [1 + 0] \\
              &= 2 \\
\end{align*}
Hier werden \(v(1, \langle \rangle)\) und \(v(0, \langle \rangle)\) doppelt berechnet.
Für größere Eingaben nimmt dieser Effekt aber deutlich zu, weshalb eine Verbesserung des Algorithmus zur maximal einmaligen Berechnung jedes Funktionswertes notwendig ist.

\subsubsection*{Der verbesserte Algorithmus}
Bei genauerer Betrachtung von Gleichung (\ref{equation:recursion}) fällt auf, dass die in der Summe zu berechnenden Anzahlen an Verteilungsmöglichkeiten stets für die selben Behälter sind und sich nur in der Anzahl der Flaschen unterscheiden.
Weiterhin handelt es sich immer um aufeinanderfolgende Anzahlen von Flaschen, welche nie die Gesamtanzahl der Flaschen (also das originale \(z\)) überschreiten.
Da es außerdem keine Verteilung einer negativen Anzahl von Flaschen geben kann, ist die kleinste zu betrachtende Anzahl an Flaschen stets größer oder gleich null.
Es ist demnach möglich aus einer Sequenz der Verteilungsanzahlen von \(0\) bis \(n\) Flaschen auf eine bestimmte Menge der Behälter eine ebensolche Sequenz zu berechnen, wobei die Menge der Behälter einen zusätzlichen Behälter enthält.

Es sei \(k\) der Index des zusätzlichen Behälters und somit \(g_k\) die maximale Anzahl von Flaschen in diesem.
Außerdem sei die Sequenz \(V(k) = \langle v(0, G(k)), v(1, G(k)), v(2, G(k)), \dots, \allowbreak v(z-1, G(k)), v(z, G(k)) \rangle\) der Anzahlen an Verteilungen von \(0\) bis \(z\) Flaschen auf die Behälter \(0\) bis \(k-1\) bereits berechnet.
Mit Gleichung~\ref{equation:recursion} ergibt sich für die Sequenz \(V(k+1)\), bei der die \(0\) bis \(z\) Flaschen zusätzlich auch auf den Behälter \(k\) verteilbar sind:
\begin{align}
  \label{equation:naive_sequence}
  V(k+1) = \langle & \sum\limits_{i = -g_k}^0 v(i, G(k+1)), \sum\limits_{i = 1-g_k}^1 v(i, G(k+1)), \ldots, \nonumber \\
                   & \ldots, \sum\limits_{i = z-1-g_k}^{z-1} v(i, G(k+1)), \sum\limits_{i = z-g_k}^z v(i, G(k+1)) \rangle \nonumber \\
  \phantom{V(k+1)} = \langle & \sum\limits_{i = -g_k}^0 V{(k)}_i, \sum\limits_{i = 1-g_k}^1 V{(k)}_i, \ldots, \sum\limits_{i = z-1-g_k}^{z-1} V{(k)}_i, \sum\limits_{i = z-g_k}^z V{(k)}_i \rangle
\end{align}
Hierbei fällt auf, dass jeweils aufeinanderfolgende Summen in der Sequenz sich nur in einem Summand unterscheiden, da die Grenzen für die Laufvariable \(i\) um eins verschoben wurden.
Es gilt offensichtlich:
\begin{align}
  \label{equation:next}
  V{(k+1)}_{l+1} &{}= \sum\limits_{i = l+1-g_k}^{l+1} V{(k)}_i \nonumber \\
                 &{}= V{(k)}_{l+1} - V{(k)}_{l-g_k} + \sum\limits_{i = l-g_k}^l V{(k)}_i \nonumber \\
                 &{}= V{(k)}_{l+1} - V{(k)}_{l-g_k} + V{(k+1)}_l
\end{align}
Mit dieser Gleichung lässt sich nun im Prinzip sehr leicht die Sequenz \(V(k+1)\) berechnen, indem die Gleichung immer wieder für das jeweils nachfolgende Glied angewendet wird.
Allerdings enthalten die Summen für die ersten \(g_k\) Elemente von \(V(k+1)\) Summanden, welche gleich den Anzahlen an Verteilungen von negativen Flaschenzahlen sind, wie leicht aus Gleichung (\ref{equation:naive_sequence}) zu erkennen ist.
Diese Verteilungsanzahlen sind stets \(0\), weshalb die entsprechenden Summanden aus der Summe entfernt werden können.
Dadurch ergibt sich für Gleichung (\ref{equation:next}), wenn \(l < g_k\) ist:
\begin{equation}
  \label{equation:next'}
  V{(k+1)}_{l+1} = V{(k)}_{l+1} + V{(k+1)}_l
\end{equation}
Zusätzlich zu den Gleichungen (\ref{equation:next}) und (\ref{equation:next'}) ist es natürlich notwendig eine Gleichung für das erste Element von \(V(k+1)\) anzugeben,
da die beiden genannten Gleichungen nur die Berechnung eines nachfolgenden Elements der Sequenz ausdrücken.
Mit Hilfe von Gleichung (\ref{equation:recursion}) ergibt sich für die Berechnung von \(V{(k+1)}_0\) aber einfach durch Weglassen von Summanden, die garantiert gleich \(0\) sind:
\begin{equation}
  \label{equation:zero}
  V{(k+1)}_0 = V{(k)}_0
\end{equation}
Die Gleichungen (\ref{equation:zero}), (\ref{equation:next'}) und (\ref{equation:next}) definieren nun einen Algorithmus zur Berechnung von \(V(k+1)\) aus \(V(k)\), es fehlt aber noch eine Gleichung für \(V(0)\) als Ausgangssequenz der Berechnung.
Unter Nutzung von Gleichung (\ref{equation:base}) folgt:
\begin{equation}
  \label{equation:base_sequence}
  V(0) = \langle 1, 0, 0, \ldots, 0, 0 \rangle
\end{equation}

Der gesamte Algorithmus läuft nun folgendermaßen ab:\\
Zunächst wird die Sequenz \(V(0)\) nach Gleichung (\ref{equation:base_sequence}) berechnet.
Anschließend werden wiederholt die Gleichungen (\ref{equation:zero}), (\ref{equation:next'}) und (\ref{equation:next}) angewendet, um die Sequenzen \(V(1)\), \(V(2)\), \dots bis \(V(n)\) zu berechnen.
Dazu wird das erste Element der neuen Sequenz stets mit Gleichung (\ref{equation:zero}) berechnet.
Die nächsten \(g_k\) Elemente werden nacheinander mit Gleichung (\ref{equation:next'}) berechnet, wobei \(g_k\) die Größe des zusätzlich betrachteten Behälters ist.
Die restlichen Elemente werden nacheinander mit Gleichung (\ref{equation:next}) berechnet.
Das letzte Element \(V{(n)}_z\) der als letztes berechneten Sequenz gibt schließlich die gesuchte Anzahl an Verteilungsmöglichkeiten an.

Eine Optimierung des Algorithmus besteht im Verzicht auf die Berechnung unnötiger Werte.
Laut Gleichung (\ref{equation:recursion}) lässt sich \(V{(n)}_z\) alleine aus den Werten der letzten \(g_{n-1} + 1\) Elemente der Sequenz \(V(n-1)\) berechnen.
Alle vorherigen Elemente von \(V(n)\) werden genauso wenig benötigt wie die Elemente vor \(V{(n-1)}_{z-1-g_{n-1}}\).
In den davor kommenden Sequenzen sind entsprechend auch Elemente am Anfang der Sequenzen für die Berechnung von \(V{(n)}_z\) überflüssig.
Dies lässt sich ausnutzen, indem die Sequenz jeweils erst ab dem benötigten Index \(j(k)\) berechnet wird.
Das Element an dieser Stelle lässt sich dann als Summe nach Gleichung (\ref{equation:recursion}) aus den Elementen der vorherigen Sequenz berechnen:
\begin{equation}
  \label{equation:start}
  V{(k)}_{j(k)} = \sum_{i=j(k-1)}^{j(k)} V{(k-1)}_i
\end{equation}
Alle weiteren Elemente der Sequenz sind genau wie vor der Optimierung zu berechnen.
Für den Index \(j(k)\) gilt:
\begin{align}
  \label{equation:index_recursive}
  j(k) &{}= \max(j(k+1) - g(n) - 1, 0) \\
  j(n) &{}= z
  \label{equation:index_base}
\end{align}
Falls der Index \(j(k)\) gleich \(0\) ist, wird \(V(k)\) wie im unoptimierten Algorithmus berechnet.

Eine weitere Verbesserung lässt sich aus der Beobachtung ableiten, dass eine Festlegung der Anzahl der frei bleibenden Plätze in jedem Behälter gleichzeitig die Anzahl der Flaschen in jedem Behälter festlegt.
Es ist somit somit exakt das selbe \(z\) Flaschen oder \(\sum G(n) - z\) freie Plätze auf die Behälter zu verteilen, da die Summe der Anzahl an Flaschen und freien Plätzen gleich der Gesamtanzahl \(\sum G(n) = \sum_{k=0}^n g_k\) aller Plätze in allen Behältern ist.
Da die Verteilung von leeren Plätzen vollkommen gleich zur Verteilung von Flaschen erfolgt, ist auch die Anzahl der möglichen Verteilungen genau gleich.
Es gilt demnach:
\begin{equation}
  \label{equation:interchange}
  v(z, G(n)) = v(\sum G(n) - z, G(n))
\end{equation}
Die Berechnung mit der kleineren der beiden Anzahlen an Flaschen liefert anschließend die bessere Laufzeit des Algorithmus.

\subsubsection*{Laufzeit und Speicherverbrauch}
Die asymptotische Laufzeit des Algorithmus beträgt \(O(\min(z, \sum G(n) - z) \times n)\).
Jedes Element von \(V(k)\) lässt sich mit Gleichung (\ref{equation:next}) oder (\ref{equation:next'}) in konstanter Zeit aus den bereits berechneten Elementen berechnen.
(Unter den Voraussetzungen, dass Addition und Subtraktion in konstanter Zeit ausführbar sind und dass der Zugriff auf die bereits berechneten Elemente der Sequenz in konstanter Zeit erfolgen kann, wie es beispielsweise bei Verwendung eines Arrays der Fall sein sollte.)
Der Algorithmus berechnet von den \(n + 1\) Sequenzen \(V(0)\) bis \(V(n)\) jeweils maximal \(z' + 1\) Elemente, wobei \(z'\) die optimierte Anzahl an Flaschen ist.
Somit ergibt sich \(O((n+1)\times(z'+1)\times1) = O(z' \times n)\).

Der asymptotische Speicherverbrauch einer optimalen Implementierung des Algorithmus beträgt \(\Theta(\min(z, \sum G(n) - z))\).
Es müssen zu jedem Zeitpunkt maximal zwei Sequenzen (\(V(k)\) und \(V(k+1)\)) gespeichert werden, welche wieder maximal \(z' + 1 = \min(z, \sum G(n) - z) + 1\) Elemente enthalten.

\subsection*{Implementierung}
Das Lösung ist in \verb|Haskell| implementiert und besteht aus den beiden Dateien \verb|task3.hs| und \verb|DistributionsCount.hs|.
Die Datei \verb|task3.hs| ist ausschließlich für das Einlesen der Eingaben und die Ausgabe der berechneten Anzahl zuständig, während das Modul \verb|DistributionsCount| die Funktion \verb|distributionsCount| definiert, welche aus der Anzahl der Flaschen und einer Liste der Behältergrößen die Anzahl der möglichen Verteilungen berechnet:
\haskellfile{task3/DistributionsCount.hs}

Die ersten beiden Zeilen der Funktion \verb|distributionsCount| überprüfen zunächst ob die Eingaben sinnvoll sind, dass heißt alle Behältergrößen sind nicht negativ und die Anzahl der Flaschen liegt zwischen null und der Anzahl aller Plätze in allen Behältern.
Sind diese Bedingungen nicht erfüllt ist keine Verteilung möglich und die Funktion gibt \(0\) zurück.
Ansonsten berechnet sie die optimierte Flaschenanzahl \verb|z'| unter Ausnutzung von Gleichung (\ref{equation:interchange}) und ruft die Unterfunktion \verb|distributionsCounts| auf.

Das erste Argument von \verb|distributionsCounts| ist der Index \(j(k)\) und das zweite Argument eine Liste von \(k\) Behältergrößen, welche der Sequenz \(G(k)\) entspricht.
Sie berechnet daraus eine im Prinzip unendliche Liste, welche mit den Werten von der Sequenz \(V(k)\) ab dem Index \(j(k)\) beginnt und sich danach entsprechend der Berechnungsvorschrift (\ref{equation:next'}) beziehungsweise (\ref{equation:next}) für das jeweils nächste Element fortsetzt.
Da \verb|Haskell| aber lazy ist, werden nur die Elemente berechnet, welche am Ende benötigt werden.
Um die Liste zu berechnen ruft sich \verb|distributionsCounts| rekursiv selber auf, wobei \(k\) um eins verringert wird.
Dazu berechnet \verb|n'| den Index \(j(k-1)\) nach Gleichung (\ref{equation:index_recursive}) und aus der Liste der Behältergrößen wird die dazu genutzte Größe entfernt, sodass die Liste der Sequenz \(G(k-1)\) entspricht.
Die vorhergehende Liste, welche der Sequenz \(V(k-1)\) ab dem Index \(j(k-1)\) entspricht, befindet sich dann in \verb|subcountList|.
Die neue Liste wird anschließend mit \verb|countList|, \verb|countList'| und \verb|countList''| zusammengesetzt.
\verb|countList| setzt die Berechnung des ersten Elements nach Gleichung (\ref{equation:start}) beziehungsweise (\ref{equation:zero}) um.
In \verb|countList'| erfolgt die Berechnung der nächsten Elemente nach Gleichung (\ref{equation:next'}) während \verb|countList''| Gleichung (\ref{equation:next}) nutzt.
Der Wert \verb|x'| steuert dabei den richtigen Einsatz der beiden letztgenannten Gleichung für die richtige Anzahl an Elementen.
Der Basisfall der Rekursion von \verb|distributionsCounts| ist erreicht, wenn eine leere Liste von Behältergröße, welche \(G(0)\) entspricht übergeben wird.
In diesem Fall ist die zurückgegebene Liste nach Gleichung (\ref{equation:base_sequence}) erzeugt.

Der Aufruf von \verb|distributionsCounts| durch \verb|distributionsCount| erfolgt mit \verb|z'| für \(j(n)\) entsprechend Gleichung (\ref{equation:index_base})
und mit der Liste, welche die Größen aller Behälter enthält und \(G(n)\) entspricht.
Dadurch wird die Liste, welche mit den Elementen der Sequenz \(V(n)\) ab dem Index \(z'\) beginnt, erzeugt.
Das erste Element gibt nun die gesuchte Anzahl der Verteilungen an und wird deshalb zurückgegeben.

\subsection*{Beispiele}
Das Programm erwartet seine Eingabe über die Standardeingabe in der selben Form wie die Dateien auf der \verb|BwInf|-Website aufgebaut sind:

Die (natürliche nicht-negative) Zahl in der erste Zeile gibt die Anzahl der Flaschen an.
Die (natürliche nicht-negative) Zahl in der zweiten Zeile gibt die (maximale) Anzahl an Behältern an.
Die durch Leerzeichen getrennten (natürlichen nicht-negativen) Zahlen in der dritten Zeile geben die Behältergrößen an.
Sind mehr Behältergrößen angegeben als die Anzahl der Behälter festlegt, werden die zusätzlichen Behältergrößen ignoriert.

Das Programm liefert für die vorgegebenen Beispieleingaben folgende Werte:
\begin{minted}{bash}
$> ./task3 < flaschenzug0.txt
2
$> ./task3 < flaschenzug1.txt
13
$> ./task3 < flaschenzug2.txt
48
$> ./task3 < flaschenzug3.txt
6209623185136
$> ./task3 < flaschenzug4.txt
743587168174197919278525
$> ./task3 < flaschenzug5.txt
4237618332168130643734395335220863408628
\end{minted}
Die benötigte Zeit zur Berechnung liegt unterhalb der menschlichen Wahrnehmungsgrenze.
