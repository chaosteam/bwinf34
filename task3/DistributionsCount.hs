module DistributionsCount where

import Data.List

distributionsCount :: Integer -> [Integer] -> Integer
distributionsCount z cs | any (< 0) cs = 0
                        | z' < 0       = 0
                        | otherwise    = head $ distributionsCounts z' cs
  where
    z' = min z $ sum cs - z

    distributionsCounts :: Integer -> [Integer] -> [Integer]
    distributionsCounts 0 []       = 1 : repeat 0
    distributionsCounts _ []       = repeat 0
    distributionsCounts n (x : xs) = countList where
      n' = max 0 $ n - x
      x' = min n x
  
      countList   = sum subcounts : countList'
      countList'  = zipWith (+) subcounts' countList ++ countList''
      countList'' = zipWith (+) (genericDrop (x - x') countList) $ zipWith (-) subcounts'' subcountList
  
      subcountList = distributionsCounts n' xs
      (subcounts, subcountList') = genericSplitAt (1 + x') subcountList
      (subcounts', subcounts'')  = genericSplitAt (x - x') subcountList'
