module Main where

import DistributionsCount

main :: IO ()
main = do
  z <- read <$> getLine
  n <- read <$> getLine
  xs <- words <$> getLine
  print $ distributionsCount z $ read <$> take n xs 
