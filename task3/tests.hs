module Main where

import Test.Tasty.QuickCheck
import Test.Tasty

import DistributionsCount

naiveDistributionsCount :: Integer -> [Integer] -> Integer
naiveDistributionsCount n (x : xs) | n >= 0 = sum [ naiveDistributionsCount (n - i) xs | i <- [0 .. x] ]
naiveDistributionsCount 0 [] = 1
naiveDistributionsCount _ _  = 0

testDistributionsCount :: NonNegative Integer -> [NonNegative Integer] -> Property
testDistributionsCount z xs = length xs <= 5 ==> distributionsCount z' xs' === naiveDistributionsCount z' xs' where
  z' :: Integer
  z' = getNonNegative z

  xs' :: [Integer]
  xs' = getNonNegative <$> xs

main :: IO ()
main = defaultMain $ testGroup "DistribiutionsCount"
  [ testProperty "distributionsCount: same result as naive implementation" testDistributionsCount ]
