﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Karte
    {
        private bool[,] loecher;
        Random random;

        public Karte(Random random)
        {
            this.random = random ;

            loecher = new bool[5, 5];
            for (int x = 0; x < 5; x++)
            {
                for(int y=0;y<5;y++)
                {
                    loecher[x, y] = random.NextDouble() < 0.5;
                }
            }
        }

        public bool getLoch(int x, int y)
        {
            return loecher[x, y];
        }

        public int errechneUbereinstimmung(Karte andere)
        {
            int ubereinN=0;//Übereinstimmung wenn diese Karte "Normal" gehalten wird
            int ubereinS = 0;//Übereinstimmung wenn diese Karte gespiegelt gehalten wird

            for (int x = 0; x < 5; x++)
            {
                for (int y = 0; y < 5; y++)
                {
                    if (loecher[x, y] == andere.getLoch(x, y))
                    {
                        ubereinN++;
                    }
                    if (loecher[4 - x, y] == andere.getLoch(x, y))
                    {
                        ubereinS++;
                    }
                }
            }
            return Math.Max(ubereinN, ubereinS);
        }

        public int errechneQuadratischeUbereinstimmung(List<Karte> karten)
        {
            int summe=0;
            foreach(Karte k in karten)
            {
                if (k != this)
                {
                    int u = errechneUbereinstimmung(k);
                    if (u == 25)
                    {
                        return Int32.MaxValue;
                    }
                    summe += u*u;
                }
            }
            return summe;
        }

        public void optimiereKarte(List<Karte> karten)
        {
            int qU = errechneQuadratischeUbereinstimmung(karten);
            for (int i = 0; i < 10; i++)
            {
                int x = random.Next(5);
                int y = random.Next(5);
                loecher[x, y] ^= true;
                int qUNeu = errechneQuadratischeUbereinstimmung(karten);
                if (qUNeu > qU)
                {
                    loecher[x, y] ^= true;
                }
            }
        }

        public String ToString()
        {
            String res = "";
            for(int y=0;y<5;y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    if (loecher[x, y])
                    {
                        res += "1";
                    }
                    else
                    {
                        res += "0";
                    }
                }
                res += "\n";
            }
            return res;
        }

    }
}
