﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            Console.WriteLine("N eingeben:");
            int N = Convert.ToInt32(Console.ReadLine());

            List<Karte> karten=new List<Karte>();
            for (int i = 0; i < N; i++)
            {
                karten.Add(new Karte(random));
            }

            DateTime time = DateTime.Now;
            int count = 0;
            while(count<10*N&&(DateTime.Now-time).TotalMilliseconds<30000)
            {
                karten[count % N].optimiereKarte(karten);
                count++;
            }
            Console.WriteLine(count);

            double sigma = 0;
            foreach(Karte k in karten)
            {
                sigma += k.errechneQuadratischeUbereinstimmung(karten)*1.0 / (2 * N * (N - 1));
            }
            Console.WriteLine("Karten: \n");

            foreach (Karte k in karten)
            {
                Console.WriteLine(k.ToString()+"\n");
            }

            Console.WriteLine("Übereinstimmung: " + sigma);
            Console.ReadLine();
        }
    }
}
